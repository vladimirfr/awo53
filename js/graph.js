(function() {
    // https://github.com/wbkd/d3-extended
    d3.selection.prototype.moveToFront = function() {  
      return this.each(function(){
        this.parentNode.appendChild(this);
      });
    };

    var callbacks = {}
    var defaultOptions = {
        magrin: {
            left: 10,
            right: 30,
            top: 10,
            bottom: 30
        },
        color: d3.scale.category20(),
        nodeSize: 5000,
        lineWidth: 1,
        fontSize: 8,
        linkDistance: 150,
        charge:-500,
        gravity: 0.5,
    };
    var prevOptions; 
    var categoryMainNodes = {}
    var possibleTags = [], possibleAttrs = [];
    var options = {
        length:175,
        color: {}
    }
    var onPause = false;
    var tagNode;
    var tagMode;
    var tags;
    var taggedAmount;
    var graph,
        adjusted = [],
        groups = [],
        clicked = false,
        showContentMode = false;
    var highlighting = false;
    var tag_grouping = false;
    var hover = false;
    var currentNodes, currentLinks;
    var timeline_nodes = {}
    var timeline_links = {}
    var currentEndTimeline = 0;
    var currentStartTimeline = 0;
    var _stopGraph = false;
    var invisible_categories = {}
    
    function is_touch_device() {
        return 'ontouchstart' in window        // works on most browsers 
            || navigator.maxTouchPoints;       // works on IE10/11 and Surface
    };
    /**
     * Checks if selected node is a category node
     */
    function checkIsMainNode(data)
    {
        var group = data.nodes[0].group;
        data.nodes[0]['isMain'] = true;
        data.nodes[0]['index'] = 0;

        for (var i = 1; i < data.nodes.length; i ++) {
            data.nodes[i]['index'] = i;
            if (group != data.nodes[i].group) {
                data.nodes[i]['isMain'] = true;
            }
            else {
                data.nodes[i]['isMain'] = false;
            }
            group = data.nodes[i].group;
        }
        return data;
    }

    /**
     * Gets all the nodes of the group, which nodeId belongs to
     */
    function getCategoryNode(nodeId) {
        var nodes = graph.nodes,
            group = graph.nodes[nodeId].group,

            result = [];

        for (var i = 0; i < nodes.length; i ++) {
            if (nodes[i].group == group) {
                result.push(nodes[i].index);
            }
        }

        return result;
    }

    /**
     * Calculates size of node in "Active nodes" mode using centrality value
     */
    function computeSizeForActiveMode(d) {
        var nodeSize = defaultOptions.nodeSize * 0.05;//* d.centrality;
        if(getNodeVisibility(d) && highlighting) {
            nodeSize = 2* defaultOptions.nodeSize * d.centrality;
        }
        if(d.isMain) {
            nodeSize = defaultOptions.nodeSize * 0.25;
        }

        d.size = nodeSize
        return nodeSize;
    }
    /** 
     * Computes marker distance from the center of node
     */
    function markerSize(d) {
        if(graph.nodes[d.target.index])
            return Math.sqrt(2*graph.nodes[d.target.index].size/Math.PI)+2;
        else return 0;
    }

    /**
     * Returns maximal centrality in the graph to pass it to the slider
     */
    function computeActiveNodeSliderValues() {
        var maxCentrality = 0
        var maxValsPerGroup = {}
        for(var i in graph.nodes) {
            var gId = graph.nodes[i].group;
            if(maxCentrality<graph.nodes[i].centrality)
                maxCentrality = graph.nodes[i].centrality;
            if(gId in maxValsPerGroup) {
                if(maxValsPerGroup[gId].max<graph.nodes[i].centrality)
                    maxValsPerGroup[gId].max = graph.nodes[i].centrality;
                if(maxValsPerGroup[gId].min>graph.nodes[i].centrality)
                    maxValsPerGroup[gId].min = graph.nodes[i].centrality;
            } else {
                maxValsPerGroup[gId] = {max:graph.nodes[i].centrality, min:graph.nodes[i].centrality};
            }
        }
        return maxCentrality;
    }
    /**
     * Computes size for usual mode
     */
    function computeSizeForDefaultMode(d) {
        var nodeSize = defaultOptions.nodeSize * 0.05//* d.centrality;
        if(d.isMain) {
            nodeSize *= 5;
        }
        d.size = nodeSize;
        graph.nodes[d.index].size = nodeSize

        return nodeSize;
    }

    /**
     * Returns array - path from current node to main node from current category
     */
    function currentToMain(from) {
        var path = []
        var currentNode = from;
        while(!currentNodes[currentNode].isMain) {
            for(var i in currentLinks) {
                var link = currentLinks[i];
                if(link.target.index == currentNode && link.source.group == link.target.group) {
                    path.push(currentNode);
                    currentNode = link.source.index;
                }
            }
        }
        path.push(currentNode);
        return path;
    }
    /**
     * Returns nodes, linked with 'nodeId' by incoming links and belong to 'group' 
     */ 
    function getNeighbors(nodeId, group) {
        var result = [],
            nodes = currentNodes,
            links = currentLinks;

        for (var i = 0; i < links.length; i ++) {
            var sourceId = links[i].source.index,
                targetId = links[i].target.index;
            if (sourceId == nodeId && group == nodes[targetId].group) {
                result.push(targetId);
            }
        }

        return result;
    }

    /**
     * Returns nodes, adjusted to the nodeId and belongs to other group than nodeId
     */
    function adjustedNodes(nodeId, outgoing) {
        var nodes = currentNodes,
            links = currentLinks,
            adjusted = [];

        for(var i=0; i<links.length; i++) {
            if(getLinkVisibility(links[i], i)) {
                if(links[i].source.index == nodeId && outgoing && links[i].target.isOriginal) {
                    if(adjusted.indexOf(links[i].target.index)==-1) {
                        adjusted.push(links[i].target.index)
                    }
                }
                if(links[i].target.index == nodeId && !outgoing && links[i].source.isOriginal) {
                    if(adjusted.indexOf(links[i].source.index)==-1) {
                        adjusted.push(links[i].source.index)
                    }
                }
            }
        }

        return adjusted;
    }

    /** 
     * Returns all the nodes, which are adjusted to nodes in adjustedArr, both dirrectly
     * or indirrect
     */
    function recursiveAdjusted(adjustedArr, newArr, outgoing) {
        var nodes = graph.nodes,
            links = graph.links,
            arr = [];

        for (var i = 0; i < newArr.length; i ++) {
            for (var j = 0; j < links.length; j ++) {
                var link = links[j];
                if(getLinkVisibility(link, j)) {
                    if(outgoing 
                        && link.source.index == newArr[i]
                        && adjustedArr.indexOf(link.target.index) == -1 
                        && arr.indexOf(link.target.index) == -1) {
                        arr.push(link.target.index);
                    } else if(!outgoing 
                        && link.target.index == newArr[i]
                        && adjustedArr.indexOf(link.source.index) == -1 
                        && arr.indexOf(link.source.index) == -1) {
                        arr.push(link.source.index);
                    }                 
                }
            }
        }
        if (arr.length > 0) {
            return recursiveAdjusted(adjustedArr.concat(newArr), arr, outgoing);
        } else return adjustedArr.concat(newArr);
    }

    // If current node is not main from category
    function getActiveNodes(nodeId) {
        var group = currentNodes[nodeId].group,
            nodes = currentNodes,
            result = [],
            toMain = [];
        result.push(nodeId);

        result = result.concat(currentToMain(nodeId));
        result = result.concat(getNeighbors(nodeId, group));

        adjusted_out = adjustedNodes(nodeId, true);
        adjusted_in = adjustedNodes(nodeId, false);

        if (adjusted_in.concat(adjusted_out).length > 0) {
            adjusted = recursiveAdjusted([], adjusted_out, true);
            adjusted = recursiveAdjusted(adjusted, adjusted_in,false);
        }

        result = removeDuplicate(result.concat(adjusted));
        for(var i = 0; i<currentLinks.length; i++) {
            var link = currentLinks[i]
            var id_1 = link.target.index;
            if(result.indexOf(id_1)!=-1 && (link.target.group == link.source.group)) {
                result = result.concat(currentToMain(id_1))
            }
        }
        return result;
    }

    // If current node is main from category
    function getActiveMainNodes(nodeId) {
        var result = getCategoryNode(nodeId),
            toMain = [],
            nodes = graph.nodes;

        if (result.length > 0) {
            for (var i = 0; i < result.length; i ++) {
                adjusted = adjustedNodes(result[i]);
            }
            if (adjusted.length > 0) {
                adjusted = recursiveAdjusted([], adjusted);
            }
        }

        result = result.concat(adjusted);

        if (adjusted.length > 0) {
            for (var i = 0; i < adjusted.length; i ++) {
                var adjustedId = adjusted[i],
                    adjustedGroup = nodes[adjusted[i]].group;

                result = result.concat(currentToMain(adjustedId, adjustedGroup));
            }
        }

        return removeDuplicate(result);
    }

    // Helper functions
    function inArray(item, arr) {
        return arr.indexOf(item)!=-1;
    }

    // Remove duplicate from array
    function removeDuplicate(arr) {
        var tmp = [];

        for (var i = 0; i < arr.length; i ++) {
            if (!inArray(arr[i], tmp)) {
                tmp.push(arr[i]);
            }
        }

        return arr;
    }

    /** Function that is performing zoom and pitch
     */
    function zoom() {
        d3.selectAll('.zoom-field').attr("transform", "translate(" + d3.event.translate + ") scale(" + d3.event.scale + ")");
    }


    Array.max = function( array ){
        return Math.max.apply( Math, array );
    };
    Array.min = function( array ){
        return Math.min.apply( Math, array );
    };
    /** Restores default functions
     */
    function defaults(obj) {
        Array.prototype.slice.call(arguments, 1).forEach(function (source) {
            if (source) {
                for (var prop in source) {
                    if (obj[prop] == null) obj[prop] = source[prop];
                }
            }
        });
        return obj;
    }

    function getIncidentNeighbors(nodeId) {
        var nodes = currentNodes,
            links = currentLinks,
            result = [nodeId];

        for (var i = 0; i < links.length; i ++) {
            if(getLinkVisibility(links[i], i)) {
                var sourceId = links[i].source.index,
                    targetId = links[i].target.index;
                if (sourceId == nodeId && links[i].target.isOriginal) {
                    result.push(targetId);
                }
                if (targetId == nodeId && links[i].source.isOriginal) {
                    result.push(sourceId);
                }
            }

        }

        return result;
    }
    /* Calculates degree centrality */
    function getDegreeCentrality(nodeId) {
        return getIncidentNeighbors(nodeId).length/graph.links.length;
    }

    /**
     * calculates eigenvector centrality for certain node under node id
     *
     * var centrality = getEigenvectorCentrality(nodeId)s
     */
    function getEigenvectorCentrality(nodeId, arr) {
        var nextNodes = []
        var centrality = 1;

        if(arr==null)
            arr = []
        for(var i in graph.links) {
            if(graph.links[i].source.index == nodeId && !(nodeId in arr) ) {
                nextNodes.push(graph.links[i].target.index)
            }
        }
        arr.push(nodeId)
        for(var i in nextNodes)
            centrality += getEigenvectorCentrality(nextNodes[i], arr);
        return centrality
    }

    /**
     * return an amount of outgoing, incoming and indirrect links
     */
    function getLinksClassificated(nodeId, targetNode, inc, arr, _links) {
        var nextNodes = []
        var links = 0;
        if(targetNode!==false) {
            // getting incomming and outgoing links
            var res = {},
                arr = [],
                in_arr = [],
                out_arr = [],
                
                outgoing = 0,
                incoming = 0;

            for(var i in graph.links) {
                if(graph.links[i].source.index == nodeId) {
                    out_arr.push(graph.links[i]);
                }
                if(graph.links[i].target.index == nodeId  && !graph.links[i].source.isMain) {
                    in_arr.push(graph.links[i]);
                }
            }
            arr.push(nodeId);
            res.incoming = in_arr.length;
            res.outgoing = out_arr.length;
            res.indirect = 0

            // invoking recursive function to pass vertexes through graph
            for(var i in out_arr) {
                res.indirect += getLinksClassificated(out_arr[i].target.index, false, false, arr)
            }
            return res
        } else {
            arr.push(nodeId)
            for(var i in graph.links) {
                if(graph.links[i].source.index == nodeId ) {
                    if(arr.indexOf(graph.links[i].target.index)==-1) {
                        links += getLinksClassificated(graph.links[i].target.index, false, false, arr);
                    }
                    links +=1
                }
            }

            return links;
        }
    }
    
    Graph.prototype.drawShowContentLinks = function(svg, links, cls) {
        var showContentLinks = svg.selectAll('.'+cls)
            .data(links)
            .enter()
                .append('g')
                .attr('class', function(d) {
                    return cls+' show-content link group_'+graph.nodes[d.source.index].group+' target-group_'+graph.nodes[d.target.index].group;
                })
                .attr('id', function(d,i) {return 'show-content-link-'+i;})
        showContentLinks
                .append('line')
                    .attr('stroke-width', 1)
                    .attr('class', 'show-content-link-to');
        showContentLinks
                .append('line')
                    .attr('stroke-width', 1)
                    .attr('class','show-content-link-from');

        svg.append('g')
            .attr('class', 'linklabels-section').style('display', 'none')
            .selectAll('text')
                .data(links)
                .enter()
                .append('text')
                    .attr('dy', 5)
                    .attr('class', function(d) {
                        return cls+' linklabels  group_'+graph.nodes[d.source.index].group+' target-group_'+graph.nodes[d.target.index].group
                    })
                    .attr('font-size', 9)
                    .attr('stroke-width', 1)
                    .attr('stroke', 'black')
                    .attr('filter', 'url(#solid)')
                    .text(function(d) {
                        var words = d.value.split(' ');
                        return words.length>2 ? words.splice(0,2).join(' ')+' ...': words.join(' ');
                    })

                    .attr('text-anchor', 'middle')                    
                    .style('fill', 'black')
                    .each(function(d) {
                        d.width = this.getComputedTextLength();
                    })

    }

    Graph.prototype.drawStraightLinks = function(svg, links, cls) {
        svg.selectAll("."+cls)
            .data(links)
            .enter()
            .append("path")
            .attr("class", function(d,i) {
                var nodes = graph.nodes;
                var index = d.source;
                var target_index = d.target;
                if(!tagMode)
                    return cls+' link straight-link group_' +nodes[index].group+ ' target-group_' + nodes[target_index].group;
                return cls+' link straight-link';
    
            })
            .style('stroke', function (d) {
                if(!tagMode) {
                    var nodes = graph.nodes;
                    var index = d.source;
                    
                    return defaultOptions.color(nodes[index].group);
                } else 
                    return '#666'
            })
            .attr('stroke-width', defaultOptions.lineWidth)
            .style("marker-end", function(d,i) {
                return !tagMode  ? 'url(#arrow_' +  currentNodes[d.source].group + ')' : 'url(#arrow_' +  d.source.group + ')';
            });
    }

    Graph.prototype.drawNodes = function(svg, nodes, cls) {
        function dragstart(d) {
            d3.selectAll('.'+cls).classed('fixed', false);
            d3.select(this).classed("fixed", d.fixed = true);
        }        
        var drag = this.force.drag()
            .on('dragstart', dragstart);
        svg.selectAll('.'+cls).each(function(d) {
            d.old = true;
        })
        var node = svg.selectAll('.'+cls)
            .data(nodes)
            .enter()
                .append('g')
                .attr('id', function(d, i) {
                    return d.isOriginal ? 'node_' + d.index : 'node_'+(graph.nodes.length+i);
                })
                .attr('class',function (d) {
                    return cls+' node group_' + d.group;
                })
                .call(drag)

        node
            .append("path")
            .attr("d", d3.svg.symbol()
                .type('circle')
                .size(computeSizeForDefaultMode))
            .attr("fill", function (d) {
                return defaultOptions.color(d.group);
            })
            .attr('stroke', '#000');
        
        node.append('text')
            .text(function(d) { return d.name/* + '(' + d.index + ')'*/; })
            .style('font-size', defaultOptions.fontSize + 'px');
        node.append('text')
            .attr('class','centrality-value')
            .attr('y', defaultOptions.fontSize)
            .style('font-size', defaultOptions.fontSize + 'px')
            .style('display', 'none')
            .text(function(d) {return d.isMain ? '' : d.centrality.toFixed(5);})

        node.filter(function(d) {
            return (d.image && d.image!=="");
        }).each(function(d) {
            d3.select(this)
                .append('image')
                .attr('xlink:href', d.image)
                .attr('width', Math.sqrt(d.size))
                .attr('x', -Math.sqrt(d.size)/2)
                .attr('y', -Math.sqrt(d.size)/2)
                .attr('height', Math.sqrt(d.size))
        })

        var timer = null
        if(!is_touch_device()) {
            node.on('mouseover', highlightNeighbors)
            node.on('mouseout', disableNeighborsHighlight)
            node.on("mousedown", function() { d3.event.stopPropagation(); })
            node.on('touchstart',function() { d3.event.stopPropagation(); })
            d3.selectAll('.linklabels').on("mousedown", function() { d3.event.stopPropagation(); })
            d3.selectAll('.linklabels').on('touchstart',function() { d3.event.stopPropagation(); })
            
            node.on('click', onClickHandler)
        } else {
            node.on('touchstart',function(d, i) {
                var self = this
                hover = true;
                timer = setTimeout(function() {highlightNeighbors(d, null, self)}, 500)
                d3.event.stopPropagation()
            })
            node.on('touchend', function(d, i) {
                if(hover) 
                    onClickHandler(d, d.index, this)
                clearTimeout(timer)
                disableNeighborsHighlight(d, d.index, this)
            })
        }
        return node;
    }

    function closeLinkLabel(d, index) {
        d3.selectAll('.linklabels').style('filter','url(#solid)')
        d3.select('#link-tooltip').style('display','none')
        if(!tagMode) 
            d3.selectAll('.show-content').style('stroke', 'black')
        else
            d3.selectAll('.show-content').style('stroke', '#aaa');

        if(callbacks.onDeselect) callbacks.onDeselect();
    } 
    
    function highlightNeighbors(d, i, context) {
        // highlighting neighbor nodes on mouseover
        var svg = d3.select('.zoom-field');
        if(!context)
            context = this;
        if(!clicked && getNodeVisibility(d)) {
            if(highlighting) {
                if(getNodeVisibility(d)) {
                    _(context);
                }
            } else _(context);
            
        }
        function _ (self) {
            var neighbors = getIncidentNeighbors(d.index);

            svg.selectAll('.node')
                .style('opacity', '0.2')
                .classed('active', false)
                .classed('hovered', false)

            for(var i in neighbors) {
                var node = d3.select('#node_'+neighbors[i]);
                if(getNodeVisibility(currentNodes[neighbors[i]])){
                    node.classed('hovered', true);
                }
            }
            
            var line = svg.selectAll('.link, .linklabels');
            line.style('opacity', function(d, i) {
                return (neighbors.indexOf(d.target.index)!=-1) && (neighbors.indexOf(d.source.index)!=-1) ? 1: 0;
            });
            svg.selectAll('.hovered')
                .style('opacity', '1');
            svg.selectAll('.hovered path')
                .attr('stroke', 'red');
        }
        hover = false;

    }

    function disableNeighborsHighlight(d, i, context) {
        // disables any effect of mouse over
        var svg = d3.select('.zoom-field');

        if(!context)
            context = this;
        if(!clicked && getNodeVisibility(d)) {
            var self = d3.select(context),
                id = d3.select(context).attr('id');

            var neighbors = getIncidentNeighbors(d.index);
            
            svg.selectAll('.node')
                .style('opacity', function(d) {
                    return (getNodeVisibility(d)) ? 1 : 0.2;
                })
            var callback = function(d, i) {
                    return getLinkVisibility(d, i) ? 1 : 0;
                }

            svg.selectAll('.straight-link')
                .style('opacity',callback)
            svg.selectAll('.show-content')
                .style('opacity', callback)
            svg.selectAll('.linklabels')
                .style('opacity', callback)

            svg.selectAll('.hovered path')
                .attr('stroke', 'black');
            for(var i in neighbors) {
                var node = d3.select('#node_'+neighbors[i]);
                node.classed('hovered', false);
            }
        }
    }

    function onClickHandler(d, i, context) {
        var svg = d3.select('.zoom-field');
        closeLinkLabel();

        if(!context)
            context = this

        if( getNodeVisibility(d) ) {
            // handles click event on active nodes or any node in usual mode 
            // disables any effect caused by previous actions
            var self = d3.select(context),
                nodeId = d.index,
                group = currentNodes[d.index].group,
                activeNodes,
                size = defaultOptions.nodeSize,
                id = d3.select(context).attr('id');

            if (adjusted.length > 0) {
                adjusted = [];
            }
            var path = d3.selectAll('.node path').attr('stroke', 'black');
            
            svg.selectAll('.node').style('opacity', function(d){
                return getNodeVisibility(d) ? 1 : 0.2;
            }).classed('active', false);

            if (clicked) {
                clicked = false;
                // if previously clicked node is clicked again
                var line = svg.selectAll('.link, .linklabels');
                redrawLinksAndNodes();
                path.attr("d", function(d) {
                    // deselcting node
                    return d3.svg.symbol(d).type(d.currentForm)
                        .size(computeSizeForActiveMode)(d)
                    })
                    //.style('stroke','black');
                d3.select('.metrics-popup').style('display', 'none');
                if(callbacks.onDeselect) callbacks.onDeselect(d);
            }
            else {
                // get the nodes connected with the clicked node
                
                if (d.isMain) {
                    activeNodes = getActiveMainNodes(nodeId);
                } else {
                    activeNodes = getActiveNodes(nodeId);
                }
                
                var path = d3.select('#' + id + ' > path');
                //change shape of clicked node
                if(path.length!=0)
                    path.attr("d", d3.svg.symbol().type('square').size(computeSizeForActiveMode));

                if (activeNodes.length > 0) {
                    // highlights selected nodes
                    svg.selectAll('.node').style('opacity', '0.2').classed('active', false);
                    svg.selectAll('.link').style('opacity', '0').classed('active', false);

                    for (var i = 0; i < activeNodes.length; i ++) {
                        var item = activeNodes[i];
                        if(getNodeVisibility(currentNodes[item])) {
                            svg.select('#node_' + item).style('opacity', '1').classed('active', true);
                        }
                    }
                    redrawLinksAndNodes(function(d, i) {
                        return activeNodes.indexOf(d.target.index)!=-1 && activeNodes.indexOf(d.source.index)!=-1 && getLinkVisibility(d, i) ? 1: 0 ;
                    }, function(d,i) {
                        return activeNodes.indexOf(d.index) !=-1 && getNodeVisibility(d)
                    })
                }
                clicked = true;
                if(d.isOriginal) {
                    var links_data = getLinksClassificated(d.index, true);
                }
                else {
                    var links_data = getLinksClassificated(d.originalIndex);
                }

                if (callbacks.onNodeClick) {
                    links_data.total = (links_data.incoming+links_data.outgoing +links_data.indirect);
                    var _d = $.extend(true, {}, d);
                    _d.centrality = _d.centrality.toFixed(6);
                    _d.linksData = links_data;
                    _d.startDate = d.startDate>0 ? d3.time.format('%Y-%m-%d')(new Date(1000*(d.startDate))) : '';
                    _d.endDate = d.endDate>0 ? d3.time.format('%Y-%m-%d')(new Date(1000*(d.endDate))) : '';

                    callbacks.onNodeClick(_d);
                }
            }
        }
    }

    Graph.prototype.setOnNodeClick = function(callback) {
        callbacks.onNodeClick = callback;
    }

    Graph.prototype.setOnLinkClick = function(callback) {
        callbacks.onLinkClick = callback;
    }

    Graph.prototype.setOnNodeDeselect = function(callback) {
        callbacks.onDeselect = callback;
    }

    /**
     * Graph constructor
     */ 
    function Graph (options) {
        defaultOptions = defaults(options, defaultOptions);
        this.data = checkIsMainNode(this.data);
        this.onPause = false;
    }

    Graph.prototype.svg;
    Graph.prototype.force;
    Graph.prototype.node;
    Graph.prototype.link;

    /**
     * inits and draws the graph svg
     */
    Graph.prototype.draw = function() {

        graph = this.data;
        d3.select('.metrics-popup').style('display', 'none')
        var sources = [];
        for (var i = 0; i < graph.links.length; i++) {
            if (!inArray(graph.links[i].source, sources)) {
                sources.push(graph.links[i].source);
            }
        }

        currentNodes = graph.nodes;
        currentLinks = graph.links;

        var svg = d3.select('#graph').append("svg")
            .attr("width", '100%')
            .attr("height", '100%')

            .call(d3.behavior.zoom().scaleExtent([0.1, 8]).on("zoom", zoom))
            .append('g')
                .attr('class', 'zoom-field')

        var force = d3.layout.force()
            .size([$(document).width(), $(document).height()])
            .charge(function(d,a) { 
                return d.charge; 
            })
            .linkDistance(defaultOptions.linkDistance)
            .gravity(defaultOptions.gravity)
            .on("tick", this.tick_straight);

        this.svg = svg;
        this.drawStraightLinks(svg, graph.links, 'links-original');

        graph.links = graph.links.map(function(d, i) {
            d.startDate = d.startDate<=0 ? 0 : d.startDate;
            d.endDate = d.endDate<=0 ? 0 : d.endDate;

            if(possibleAttrs.indexOf(d.value)==-1)
                possibleAttrs.push(d.value)            
            return d;
        })
        graph.nodes = graph.nodes.map(function(d,i) {
            if(!d.isMain) {
                d['charge'] = -defaultOptions.charge;
            } else { 
                categoryMainNodes[d.group] = i;
                d['charge'] = -defaultOptions.charge*2;
            }
            if(groups.indexOf(d.group)==-1)
                groups.push(d.group);
            
            d.startDate = d.startDate<=0 ? 0 : d.startDate;
            d.endDate = d.endDate<=0 ? 0 : d.endDate;

            return d;
        });

        force
            .nodes(graph.nodes)
            .links(graph.links)
            .start();

        this.force = force;

        /* making initial state of nodes */
        graph.nodes = graph.nodes.map(function(d,i) {
            d['degreeCentrality'] = getDegreeCentrality(d.index);
            if(d.isMain) {
                d.centrality = 0
            }
            else {
                d['centrality'] = getEigenvectorCentrality(d.index)/graph.links.length;
            }
            d['currentForm'] = 'circle';
            d.isOriginal = true;

            d.visible = true;

            if(!('tags' in d))
                d.tags = [];
            
            for(var i=0; i<d.tags.length; i++) {
                if(possibleTags.indexOf(d.tags[i])==-1) {
                    possibleTags.push(d.tags[i]);
                }
            }

            if(!('attrTags' in d))
                d.attrTags = [];
            return d;
        });

        var maxCentrality = computeActiveNodeSliderValues();
        defaultOptions.active = maxCentrality * graph.links.length/2 
        
        var relations = [];
        var links = graph.links.slice();

        // computing an order of links in case when two or more links between
        // two nodes exists
        for(var i=0; i<links.length; i++) {
            var arr = [i]
            links[i].linknum = 0;
            links[i].visible = true; 
            links[i].opacity = 1;

            for(var j=i-1; j>=0; j--) {
                if((links[j].target.index==links[i].target.index) && (links[j].source.index==links[i].source.index) ||
                    (links[j].target.index==links[i].source.index) && (links[j].source.index==links[i].target.index)){
                    arr.push(j)
                } 
            }
            for(var j in arr) {
                links[arr[j]].linknum += 1;
                links[arr[j]].sameLinks = arr.length;
            }
            var target = links[i].target.index;
            var source = links[i].source.index;
            
            if(links[i].value != '' && currentNodes[target].attrTags.indexOf(links[i].value)==-1)
                currentNodes[target].attrTags.push(links[i].value);
    
            if(currentNodes[source].attrTags.indexOf(links[i].value)==-1)
                currentNodes[source].attrTags.push(links[i].value);
        }

        graph.labeled_links = graph.links.reduce(function(a,b) {
            if (b.value.length>0) { 
                a.push(b); 
                return a;
            } else {
                a.concat([]); 
                return a;
            }}, []);
        
        var defs = svg.append("defs")
        
        this.drawShowContentLinks(svg, graph.links, 'show-content-original');

        var filter = defs.append('filter')
            .attr('x',"0")
            .attr('y',"0")
            .attr('width',"1")
            .attr('height',"1")
            .attr('id',"solid")
        filter.append('feFlood')
            .attr('flood-color',"#DAA520")
        filter.append('feComposite').attr('in',"SourceGraphic")

        var selectedLabel = defs.append('filter')
            .attr('x',"0")
            .attr('y',"0")
            .attr('width',"1")
            .attr('height',"1")
            .attr('id',"selected_label")
        
        selectedLabel.append('feFlood')
            .attr('flood-color',"red");

        selectedLabel.append('feComposite').attr('in',"SourceGraphic")
        
        var node = this.drawNodes(svg, graph.nodes, "node-original");

        svg.selectAll('.linklabels').on('click', function(d, index){
            // handling of click on link content

            // to change link tooltip logic, modify the code below
            var data = {};
            data['From'] = d.source.name;
            data['To'] = d.target.name;
            data['Value'] = d.value;

            var startDate = d.startDate>0 ? d3.time.format('%Y-%m-%d')(new Date(d.startDate*1000)): ''; 
            var endDate = d.endDate>0 ? d3.time.format('%Y-%m-%d')(new Date(d.endDate*1000)): ''; 
            
            data['Start date'] = startDate;
            data['End date'] = endDate;
            svg.selectAll('.linklabels').style('filter','url(#solid)');
            d3.select(this).style('filter', 'url(#selected_label)');

            if(callbacks.onLinkClick) callbacks.onLinkClick(data);
        })

        d3.select('.close-button').on('click', closeLinkLabel)
    
        defs.selectAll(".marker")
            .data(groups)
            .enter()
            .append("marker")
            .attr("viewBox", "0 -5 10 10")
            .attr("id", function (d, i) {
                return 'arrow_' + d;
            })
            .attr("refX", 50)
            .attr("refY", 0)
            .attr("markerWidth", 10)
            .attr("markerHeight", 10)
            .attr('markerUnits', 'userSpaceOnUse')
            .attr("orient", "auto")
            .append("path")
            .attr("d", "M0,-7L7,0L0,5 L7,0 L0, -5")
            .attr('class', function(d) {
                return 'marker group_' + d ;
            })
            .style("stroke", function(d) {
                return defaultOptions.color(d);
            })
            .style("opacity", "1");

        defs.selectAll('.marker-show-content')
            .data(graph.links)
            .enter()
            .append('marker')
            .attr("viewBox", "0 -5 10 10")
                .attr("id", function (d, i) {
                    return 'show-content-arrow_' + i;
                })
                .attr("refX", markerSize)
                .attr("refY", 0)
                .attr("markerWidth", 10)
                .attr("markerHeight", 10)
                .attr('markerUnits', 'userSpaceOnUse')
                .attr("orient", "auto")
                .attr('class','marker-show-content')
                .append("path")
                    .attr("d", "M10,0 L0,5 L0,-5 L10, 0")
                    .attr('class', function(d) {
                        var nodes = graph.nodes;
                        return 'group_' + nodes[d.source.index].group;
                    })
                .style("stroke", 'black')
        
        function dblclick(d) {
            d3.select(this).classed("fixed", d.fixed = false);
        }
        
        $('svg').click(function(event) {
            // disables any selections            
            var target = $(event.target);

            if (target.closest('.node').length == 0 && d3.selectAll('.node.active').size() > 0) {

                redrawLinksAndNodes();

                svg.select('.node-metric').style('display', 'none')

                var path = d3.selectAll('.node path');

                path.attr("d",function(d) {
                    return d3.svg.symbol().type(d.currentForm)
                        .size(computeSizeForActiveMode)(d)
                    }
                );
                d3.select('.metrics-popup').style('display', 'none')
                clicked = false;
                // invokes onDeselect callback if it's defined
                if(callbacks.onDeselect) callbacks.onDeselect();
            }
        });

    }

    /**
     * performs nodes mothions
     */ 
    function transform(d) {
        return "translate(" + d.x + "," + d.y + ")";
    }

    function tagModeTick() {
        var nodes = tags;
        var tagNum = 0;
        var tagged = 0;
        
        for(var i=0; i<nodes.length; i++) {
            if(nodes[i].isTag) {
                var r = computeCircle({x: tagNode.x, y: tagNode.y}, tagNum++, nodes.length-1,defaultOptions.linkDistance*2)
                nodes[i].x = r.x;
                nodes[i].y = r.y;
            }
        }

        var a = 0
        for(var i=0; i<graph.nodes.length; i++) {
            if(!('tagged' in graph.nodes[i]) && !graph.nodes[i].isMain) {
                var r = computeCircle({x: tagNode.x, y: tagNode.y}, a++, graph.nodes.length-groups.length-taggedAmount,defaultOptions.linkDistance*3)
                graph.nodes[i].x = r.x;
                graph.nodes[i].y = r.y;
            }
        }
    }

    /**
     * Tick function for usual graph representation form
     */
    Graph.prototype.tick_straight = function(force) {
        if(tagMode) {
            tagModeTick()        
        }
        d3.selectAll('.straight-link')
            .attr('d', function(d) {
                return 'M'+d.source.x+','+d.source.y+'L'+d.target.x+','+d.target.y;
            })

        d3.selectAll('.node').attr("transform", transform);
        placeTags();
    }

    /**
     * Tick function for "Show Content" mode
     */
    Graph.prototype.tick_show_content = function() {
        if(tagMode) {
            tagModeTick();
        }
        labels = [];

        d3.selectAll('.show-content-link-from')
            .attr('x1', function(d) {return d.source.x})
            .attr('y1', function(d) {return d.source.y})
            .attr('x2', function(d) {return d.x})
            .attr('y2', function(d) {return d.y});

        d3.selectAll('.show-content-link-to')
            .attr('x1', function(d) {return d.x})
            .attr('y1', function(d) {return d.y})
            .attr('x2', function(d) {return d.target.x})
            .attr('y2', function(d) {return d.target.y})
            .attr('marker-end', function(d, i) {
                return 'original' in d ? 'url(#show-content-arrow_'+d.original+')' : 'url(#show-content-arrow_'+i+')';
            });

        d3.selectAll('.linklabels')
            .each(initLabels);

        d3.selectAll('.linklabels')
            .each(collision)
            .attr('transform', function(d) {
                return "translate(" + d.x + "," + d.y + ")";
            });
            
        d3.selectAll('.node').attr("transform", function(d) {
            return "translate(" + d.x + "," + d.y + ")";
        });
        placeTags();
    };

    function placeTags() {
        var vals = {};
        d3.selectAll('.node')
            .each(function(d) {
                if(!(d.tagged in vals)) {
                    vals[d.tagged] = {y:d.y, sumX:0, count: 0}
                }
                vals[d.tagged].y = Math.max(vals[d.tagged].y, d.y);
                vals[d.tagged].sumX += d.x;
                vals[d.tagged].count++;
            })
        d3.selectAll('.tags')
            .attr('transform', function(d) {
                var data = vals[d.data];
                if(data) {
                    return 'translate('+[data.sumX/data.count, data.y+25]+')';
                } else {
                    return 'translate('+[d.x, d.y]+')';
                }
            })
    }

    /**
     * sets x and y coordinates for link labels in Show Content mode
     */
    function initLabels(d) {
        var x0 = ((d.source.x+d.target.x)/2),
            y0 = ((d.source.y+d.target.y)/2);
        //y = (x-x0)(x2-x1)/(y2-y1)+y0
        var x = x0+1
        var y = -((x-x0) * (d.target.x - d.source.x))/(d.target.y - d.source.y)+y0;

        var vec = {
            x: x-x0,
            y: y-y0            
        }
        vec = {
            x: vec.x/Math.sqrt(vec.x*vec.x+vec.y*vec.y),
            y: vec.y/Math.sqrt(vec.x*vec.x+vec.y*vec.y)
        }
        d.x = x0+vec.x*10*(d.linknum-d.sameLinks);
        d.y = y0+vec.y*10*(d.linknum-d.sameLinks);
    }

    /**
     * Calculates collision detecting between linklabels
     */
    function collision(d) {
        var x3 = d.x+d.width/2+10,
            y3 = d.y+25,
            x0 = d.x-d.width/2+10,
            y0 = d.y-25
        var quadtree = d3.geom.quadtree(graph.labeled_links);

        quadtree.visit(function(node, x1,y1, x2,y2) {
            if(node.point && (node.point!==d)) {
                var dy = Math.abs(node.point.y-d.y)+1,
                    dx = Math.abs(node.point.x-d.x)+1;
                if(dx<=node.point.width/2+d.width/2 && dy<=10) {
                        var delta = (10-dy)/2+2
                        //adjusting node position if collision is detected
                        if (node.point.y>d.y){
                            node.point.y += delta;
                            d.y -= delta
                        } else {
                            node.point.y -= delta;
                            d.y += delta
                        }
                }

            }
            return x1 > x3 || x2 < x0 || y1 > y3 || y2 < y0;
        })
    }

    /**
     * Turns "Active Node" mode on
     */
    Graph.prototype.highlightActiveNodes = function() {
        var self = this;

        highlighting = true;
        /*var maxCentrality = computeActiveNodeSliderValues();
        $('#centrality-active-node').slider({
            min: 0,
            max: maxCentrality * graph.links.length,
            value: defaultOptions.active,
        });
        d3.select('#current-active-value').text((defaultOptions.active/graph.links.length).toFixed(5));
        */
        highlight();
    }
    
    /**
     * Returns maximal value of node centrality
     */
    Graph.prototype.getMaximalCentralityValue = function() {
        return graph.links.length * computeActiveNodeSliderValues();
    }

    /**
     * Function, that performs actives node highlighting
     */
    function highlight() {
        var activeNodes = []
        d3.selectAll('.node>path')
            .attr('d', d3.svg.symbol()
                .type('circle')
                .size(computeSizeForActiveMode))
        d3.selectAll('.node>image')
            .each(function(d) {
                var self = d3.select(this);
                var _sz = Math.sqrt(computeSizeForActiveMode(d));
                self
                    .attr('x',-_sz/2)
                    .attr('y',-_sz/2)
                    .attr('width',_sz)
                    .attr('height',_sz)

            })
        d3.selectAll('.centrality-value').style('display', null)

        // setting up visible mode for appropriate nodes
        redrawLinksAndNodes();

        d3.selectAll('.marker-show-content')
            .attr('refX', markerSize)
    }
    /**
     * Turns "Active nodes" mode off. Switches to a usual mode 
     */
    Graph.prototype.highlightAsUsual = function() {
        highlighting = false;
        d3.selectAll('.node>path')
            .attr("fill", function (d) {
                if('.group_'+d.group in options.color)
                    return options.color['.group_'+d.group];
                else
                    return defaultOptions.color(d.group);
            })

        redrawLinksAndNodes();

        d3.selectAll('.centrality-value').style('display', 'none')
        d3.selectAll('.node>path')
            .each(function(d) {
                d.currentForm='circle';
                d3.select(this)
                    .attr('d', d3.svg.symbol()
                        .type(d.currentForm)
                        .size(computeSizeForDefaultMode))
            })
    }
    /**
     * Switches "show content" mode. If t == true then show content mode on,
     * otherwise, it is off
     */
    Graph.prototype.showContent = function(t) {
        if(t) {
            this.tick_show_content()
            showContentMode = true
            d3.selectAll('.straight-link')
                .attr('display', 'none');
            d3.selectAll('.linklabels-section')
                .style('display', null)
            d3.selectAll('.show-content')
                .attr('display', null)
                .style('stroke', '#666')
            this.tick_show_content()
                
            this.force.on('tick', this.tick_show_content)
        } else {
            showContentMode = false;

            d3.selectAll('.show-content')
                .attr('display', 'none');
            d3.selectAll('.linklabels-section')
                .style('display', 'none')
            d3.selectAll('.straight-link')
                .attr('display', null)
                .attr("d", function(d) { 
                    return 'M'+[d.source.x, d.source.y]+'L'+[d.target.x,d.target.y]; 
                })

            this.force.on('tick', this.tick_straight);
        }
        redrawLinksAndNodes();
    }

    // Graph manipulation
    Graph.prototype.changeNodeSize = function(size) {
        defaultOptions.nodeSize = size;
        d3.selectAll('.node').each(function(d) {
            var self = d3.select(this),
                id = self.attr('id'),
                type = 'circle';

            if(d3.select(this).classed('fixed')) {
                type = 'square';
            }

            var path = d3.select('#' + id + ' path');
            path.attr("d", d3.svg.symbol()
                    .type(function(d) {
                        return d.currentForm;
                    })
                    .size(computeSizeForActiveMode)
            );
            var img = d3.select('#'+id+' image')
            if(img[0][0]) {
                path.remove();
                var sz = Math.sqrt(computeSizeForActiveMode(self[0][0].__data__));
                img.attr('x', -sz/2)
                    .attr('y', -sz/2)
                    .attr('width', sz)
                    .attr('height', sz)
            }

            d3.selectAll('.marker-show-content')
                .attr('refX', markerSize)
        });
    }

    Graph.prototype.stopGraph = function() {
        this.onPause = true;
        d3.selectAll('.node')
            .each(function(d){
                d.fixed = true;
            })
    };

    Graph.prototype.startGraph = function() {
        this.onPause = false;
        d3.selectAll('.node')
            .each(function(d){
                d.fixed = false;                
            })
    };

    /**
     * Updates links width according to the slider
     */
    Graph.prototype.changeLineWidth = function(width) {
        defaultOptions.lineWidth = width

        d3.selectAll('.straight-link')
            .style('stroke-width', width)
        d3.selectAll('.show-content line')
            .attr('stroke-width', width)
    }
    /**
     * Updates font on node labels according to the slider
     */
    Graph.prototype.changeFontSize = function(size) {
        defaultOptions.fontSize = size;
        d3.selectAll('.node > text').style('font-size', size + 'px');
        d3.selectAll('.centrality-value').attr('y', size);
    }
    /**
     * Updates the length of links
     */
    Graph.prototype.changeLineLength = function(length) {
        defaultOptions.linkDistance = length;
        if(!tagMode)
            this.force.linkDistance(length).start();
        else 
            this.force.linkDistance(function(d) {
                if(d.source.isTag)
                    return defaultOptions.linkDistance/15;
                return Math.min(defaultOptions.linkDistance, 150);
            }).start();

    }
    /**
     * Changes a charge of nodes
     */
    Graph.prototype.changeCharge = function(charge) {
        defaultOptions.charge = charge;
        this.svg.selectAll('.node').each(function(d) {
            if(d.isMain) {
                d.charge = -charge
            } else {
                d.charge = -2*charge;
            }
        })
        this.force
            .charge(function(d){
                return d.charge ? d.charge : defaultOptions.charge;
            })
            .start()
    }
    /**
     * Changes gravity of graph
     */
    Graph.prototype.changeGravity = function(gravity) {
        this.force.gravity(gravity).start();
    }

    /**
     * Returns a list of categories
     */
    Graph.prototype.getCategories = function() {
        var categories = [];
        if (this.data.nodes.length > 0) {
            var group = this.data.nodes[0].group;
            categories.push(this.data.nodes[0].name);

            for (var i = 0; i < this.data.nodes.length; i ++) {
                if (this.data.nodes[i].group != group) {
                    group = this.data.nodes[i].group;

                    categories.push(this.data.nodes[i].name);
                }
            }
        }

        return categories;
    }

    /**
     * @argument listOfIndices - list of indeces of nodes which visibility needs to be changed
     * @argument toogle - boolean which sets if nodes are visible or not
     */
    Graph.prototype.setNodesVisibility = function(listOfIndices, toogle, listOfTags) {
        for(var i in listOfIndices) {
            if(!listOfTags) {
                graph.nodes[listOfIndices[i]].visible = toogle; 
            } else {
                // if tags handling is required
                if(listOfTags.indexOf(graph.nodes[listOfIndices[i]].tagged) != -1)
                    graph.nodes[listOfIndices[i]].visible = toogle; 

            }
            for(var k in currentNodes) {
                if(listOfTags) {
                    // if tags handling is required
                    if(currentNodes[k].originalIndex == listOfIndices[i] &&
                        listOfTags.indexOf(currentNodes[k].tagged) != -1)

                        currentNodes[k].visible = toogle;

                } else if(currentNodes[k].originalIndex == listOfIndices[i])
                    currentNodes[k].visible = toogle;
            }
        }

        redrawLinksAndNodes();

        if(highlighting) {
            this.highlightAsUsual();
            this.highlightActiveNodes();
        }
    }


    /**
        Swithces category visibility
     */
    Graph.prototype.setCategoryVisibility = function(cId, toogle) {
        if(!toogle) {
            invisible_categories[cId] = true;
            // if visible
        } else {
            // if invisible
            delete invisible_categories[cId]
        }
        redrawLinksAndNodes();

        if(highlighting) {
            this.highlightAsUsual();
            this.highlightActiveNodes();
        }
    }

    var getNodeVisibility = function(node) {
        var result = true
        if(highlighting) {
            result = (result && node.centrality>=defaultOptions.active/graph.links.length) || node.isMain
        }
        if(tagMode) {
            result = result && !node.isMain;
        }
        result = result && !(node.group in invisible_categories) && node.visible;
        result = result && (!node.startDate || node.startDate<=0 || (node.startDate>=currentStartTimeline && node.startDate<=currentEndTimeline));
        return result ;
    }

    var getLinkVisibility = function(link, i) {
        return getNodeVisibility(link.target) && 
               getNodeVisibility(link.source) &&  
               (!link.startDate || link.startDate<=0 || (link.startDate>=currentStartTimeline && link.startDate<=currentEndTimeline));
    }

    /**
     * sets values for "active nodes" modey
     */
    Graph.prototype.setActiveValue = function(value) {

        defaultOptions.active = value;
        d3.select('#current-active-value').text((defaultOptions.active/graph.links.length).toFixed(5));
        if(highlighting) highlight()
        //graph.setActiveValue(size);
    }

    /**
     * Changes color for certain group
     */
    Graph.prototype.changeCategoryColor = function(color, group) {
        d3.selectAll(group + ' > path').attr('fill', color);
        d3.selectAll('.straight-link' + group).style('stroke', color);
        d3.selectAll('.marker' + group).style('stroke', color);
        options.color[group] = color;
        if(highlighting) {
            this.highlightAsUsual();
            this.highlightActiveNodes();
        }
    }
    
    /**
     * Enables or disables all categories 
     */
    Graph.prototype.switchAllCategories = function(toogle) {
        if(toogle) {
            for(var i in invisible_categories) {
                this.setCategoryVisibility(i, true);
            }
        } else {
            var categories = this.getCategories();
            for(var i=0; i<categories.length; i++) {
                this.setCategoryVisibility(i, false);
            }
        }
    }

    /**
     * Returns list of two element, which are minimal and maximal values
     * for date in the graph
     */
    Graph.prototype.getTimeExtent = function() {
        var e1 = d3.extent(graph.nodes, function(d, i) {
            return d.startDate!=0 ? d.startDate : undefined;
        })
        var e2 = d3.extent(graph.links, function(d, i) {
            return d.startDate!=0 ? d.startDate : undefined;
        })
        var e3 = d3.extent(graph.nodes, function(d, i) {
            return d.endDate!=0 ? d.endDate : undefined;
        })
        var e4 = d3.extent(graph.links, function(d, i) {
            return d.endDate!=0 ? d.endDate : undefined;
        })
        var extent = d3.extent(e1.concat(e2).concat(e3).concat(e4), function(d){return d;});
        var _min = new Date(0);
        var _max = new Date(0);
        currentEndTimeline = extent[1];
        currentStartTimeline = extent[0];

        var minYear = (new Date(extent[0]*1000)).getFullYear()-1;
        var maxYear = (new Date(extent[1]*1000)).getFullYear()+1;
        _min.setFullYear(minYear);
        _max.setFullYear(maxYear);

        return {extent: [_min.getTime()/1000, _max.getTime()/1000], years: [minYear, maxYear]};
    }

    /**
     * Sets the timeline range for graph:
     *
     * @time - array of two values, which mean [minTime, maxTime]
     */
    Graph.prototype.setTimeline = function(time) {
        currentEndTimeline = time[1];
        currentStartTimeline = time[0];

        for(var i in graph.nodes) {
            if(!graph.nodes[i].endDate || graph.nodes[i].endDate<=0 || graph.nodes[i].endDate>=currentEndTimeline) {
                delete timeline_nodes[i];
            }
            else timeline_nodes[i] = false ;
        }
        for(var i in graph.links) {
            if(!graph.links[i].endDate || graph.links[i].endDate<=0 || graph.links[i].endDate>=currentEndTimeline)
               delete timeline_links[i];
            else timeline_links[i] = false ;
        }
        var self = this;
        redrawLinksAndNodes();
        
        if(highlighting) {
            d3.selectAll('.node>path')
                .attr('d',function(d) {
                    return d3.svg.symbol(d).type(d.currentForm)
                        .size(computeSizeForActiveMode)(d)
                })
        }
    }
    /**
     * Put this function to redraw graph if you are making any changes in representation or 
     * visibility of link or nodes
     *
     * @param f - callback custom function that setups custom node visibility behavior
     * @param f1 - callback custom function that setups custom link visibility behavior
     */
    function redrawLinksAndNodes (f, f1) {
        var callback_node = f1 ? f1 : function(d) {
            return getNodeVisibility(d) ? 1: 0.2; 
        }
        var callback_links = f ? f : function(d, i) {
            return getLinkVisibility(d, i) ? 1: 0; 
        }
            
        d3.selectAll('.node')
            .style('opacity',callback_node)

        d3.selectAll('.node>path')
            .style('stroke', function(d, i) {
                return !(i in timeline_nodes) ? "black": "red" 
            })
            .style('stroke-width', function(d, i) {return !(i in timeline_nodes) ? '1px': "2px" })

        d3.selectAll('.straight-link')
            .style('opacity', callback_links)
            .style('stroke', function(d, i) {
                return !(i in timeline_links) ? getLinkColor(d): "red" 
            })
            .style('stroke-dasharray', function(d, i) {return !(i in timeline_links) ? null: "5,5" });
        
        d3.selectAll('.show-content')
            .style('opacity',callback_links)

        d3.selectAll('.linklabels')
            .style('opacity',callback_links);
        
        d3.selectAll('.show-content line')
            .style('stroke', function(d, i) {
                if(!(graph.links.indexOf(d) in timeline_links)) {
                    if(tagMode) return '#aaa'; 
                    else return 'black';  
                } else return "red"
            })
    }
    
    function computeCircle (center, i, vertexNum, radius) {
        return {
            x: radius * Math.cos(i*(2*Math.PI/vertexNum))+center.x,
            y: radius * Math.sin(i*(2*Math.PI/vertexNum))+center.y,
        }
    }

    function getLinkColor(d) {
        return tagMode ? '#bbb': defaultOptions.color(d.source.group);
    }

    Graph.prototype.switchAttrMode = function(_tags) {
        this.switchTagMode(_tags, true)
    }

    Graph.prototype.refuseTagging = function(toogle) {

        this.force.nodes(graph.nodes);
        this.force.links(graph.links);
        this.force.charge(-defaultOptions.charge)
        this.force.gravity(defaultOptions.gravity);
        this.force.linkDistance(defaultOptions.linkDistance);
        currentLinks = graph.links;
        currentNodes = graph.nodes;
        defaultOptions = prevOptions;
        d3.selectAll('.show-content').style('stroke', '#666');

        d3.selectAll('.tags').remove();
        d3.selectAll('.node-clone').remove();
        d3.selectAll('.link-cloned').remove();
        d3.selectAll('.show-content-cloned').remove();
        d3.selectAll('.linklabel-cloned').remove();

        d3.selectAll('.node')
            .filter(function(d){return d.isMain})
                .style('display', function(d) {
                    return null;
                })

        this.force.start();
        tagMode = false;

        if(!toogle)
            redrawLinksAndNodes();
    } 

    /**
     * Switches on tag display mode
     */
    Graph.prototype.switchTagMode = function(inTags, attrMode) {
        this.force.stop();

        if(tagMode) {
            this.refuseTagging(true);
        }
        tagMode = true;
        
            taggedAmount = 0;

            var nodes = inTags;
            
            d3.selectAll('.tags').remove();

            var shift = graph.nodes.length
            var _tags = {};

            for(var i in nodes) {
                nodes[i] = {
                    data: nodes[i], 
                    isTag:true, 
                    x:0, 
                    y:0
                };

                _tags[nodes[i].data] = i;
            }
            d3.select('.zoom-field').selectAll('.tags').data(nodes).enter()
                .append('text')
                    .classed('tags', true)
                    .attr('x', function(d) {return d.x})
                    .attr('y', function(d) {return d.y})
                    .attr('font-size', 20)
                    .text(function(d){return d.data})
            tagNode = {x:0, y:0, data:"MAIN", isRoot: true}
            nodes.push(tagNode);
            tags = nodes;

            _nodes = graph.nodes;
            cloned = []

            var _tags_keys = Object.keys(_tags) 
            var _links = [];
            for(var i in graph.nodes) {
                var target = +i;
                delete graph.nodes[i].tagged;
                if(!_nodes[i].isRoot && !_nodes[i].isMain) {
                    var node = graph.nodes[i];
                    var original = true;
                    node.fixed = false;

                    if(!attrMode) var tags_ = node.tags;
                    else var tags_ = node.attrTags;

                    for(var j=0; j<tags_.length; j++) {
                        if( _tags_keys.indexOf(tags_[j]) != -1) {
                            if(original) {
                                taggedAmount++;
                                node.tagged = tags_[j];
                                original = false;  
                            } else {
                                shift++;
                                var toAdd = $.extend(true, {}, node);
                                toAdd.isOriginal = false;
                                toAdd.originalIndex = node.index
                                toAdd.tagged = tags_[j];
                                cloned.push(toAdd);
                            }
                        }
                    }
                }
            }

            _nodes = _nodes.concat(cloned)
            for(var i in _nodes) {
                var target = +i ;
                if('tagged' in _nodes[i] && (_nodes[i].tagged in _tags)) {
                    var source = +_tags[ _nodes[i].tagged ] + +shift;
                    _links.push({target: target, source: source});
                }
            }

            nodes = _nodes.concat(nodes)
            var clonedLinks = [];
            for(var i=0; i<cloned.length;i++) {
                for(var j=0; j<graph.links.length; j++) {
                    var link = graph.links[j]
                    var copiedLink = $.extend(true, {} , graph.links[j]);
                    if(link.target.index == cloned[i].index) {
                        copiedLink.target = cloned[i];
                        copiedLink.source = link.source;
                        copiedLink.original = j;
                        clonedLinks.push(copiedLink);
                    } else if(link.source.index == cloned[i].index) {
                        copiedLink.source = cloned[i];
                        copiedLink.target = link.target;
                        copiedLink.original = j;
                        clonedLinks.push(copiedLink);
                    }
                } 
            }

            d3.selectAll('.show-content').style('stroke', '#aaa');
            d3.selectAll('.tags')
                .attr('text-anchor', 'middle')
                .style('fill', 'rgba(#000)')
                .style('stroke-width', '1px')
                .style('stroke', 'rgba(0,0,0,0.7)')

                .moveToFront();
                
            this.force.nodes(nodes);
            
            this.drawStraightLinks(this.svg, clonedLinks, 'link-cloned');
            this.drawShowContentLinks(this.svg, clonedLinks, 'show-content-cloned');
            this.drawNodes(this.svg, cloned, 'node-clone');

            d3.selectAll('.linklabels-section').moveToFront();
            d3.selectAll('.node')
                .moveToFront()
                .filter(function(d){return d.isMain})
                    .style('display', function(d) {
                        return 'none';
                    })

            prevOptions = $.extend(true, {}, defaultOptions);
            defaultOptions.linkDistance = 150;

            this.force.linkDistance(function(d) {
                if(d.source.isTag)
                    return defaultOptions.linkDistance/15;
                return Math.min(defaultOptions.linkDistance, 150);
            });

            currentLinks = graph.links.concat(clonedLinks);
            currentNodes = graph.nodes.concat(cloned);

            this.force.links(_links);
            this.force.gravity(0.1);
            this.force.charge(function(d) {
                if(d.isTag)
                    return -150
                return -defaultOptions.charge/4; 
            });

            this.force.start();

        redrawLinksAndNodes();
    }

    Graph.prototype.getPossibleTags = function () {
        return possibleTags;
    }

    Graph.prototype.getPossibleAttrs = function () {
        return possibleAttrs;
    }

    Graph.prototype.getLinksClassificated = function(nodeId) {
        return getLinksClassificated(nodeId).indirect;
    }

    Graph.prototype.clear = function () {
        d3.select('#graph svg').remove();
    }
    /**
     * Graph data json
     */
    Graph.prototype.data = data

    window.Graph = Graph;
})();