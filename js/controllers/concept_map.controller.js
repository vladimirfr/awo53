angular
    .module('conceptMap')
    .controller('ConceptMapController', ConceptMapController);
 
function ConceptMapController($scope) {
    var vm = this;
    var currentPosition = 0;
    var percentage;

    function shiftLeft() {
        if(currentPosition<0) {
            currentPosition+=30;
        }

        $('#timeline').animate({'left': currentPosition+'%'});
    }

    function changeRange(start, end, timelineMode) {
        vm.timeline = yearSelector(start, end, timelineMode);

        if(currentPosition-90<=-percentage) {
            currentPosition = -percentage+90;
            $('#timeline').animate({'left': currentPosition+'%'});
        }

        var extent = [vm.timeline.options.stepsArray[vm.valueBottom].value, vm.timeline.options.stepsArray[vm.valueTop].value];

        vm.graph.setTimeline(extent);
    }
    
    function shiftRight() {
        if(currentPosition-90>-percentage) {
            currentPosition-=30;
        }

        $('#timeline').animate({'left': currentPosition+'%'});
    }

    function yearSelector(first, last, mode) {
        var _first = (new Date(0));
        var _last = (new Date(0));

        _first.setFullYear(first);
        _first = _first.getTime()/1000;
        _last.setFullYear(last);
        _last = _last.getTime()/1000;

        var stepsArray = [], years = [];

        if(vm.timeline) {
            var prevValueBottom = vm.timeline.options.stepsArray[vm.valueBottom].value;
            var prevValueTop = vm.timeline.options.stepsArray[vm.valueTop].value;
        }

        percentage = (30*(last-first));

        for(var year=first, index = 0; year<=last; year++) {
            if(mode === 'quarter') {
                console.log(prevValueBottom+' ');
                for(var quarter=0; quarter<=3; quarter++, index++) {
                    var value = (new Date(year, quarter*3, 1)).getTime()/1000;
                    
                    if(prevValueBottom >= value) {
                        console.log(prevValueBottom+' '+value)
                        vm.valueBottom = index;
                    }
                    if(prevValueTop <= value) {
                        vm.valueTop = index;  
                    } 
                    var toPush = {
                        value:value,
                        quarters : true,
                        label: 'Q'+(quarter+1),
                        year: year,
                    };
                    if(quarter == 0) toPush['yearLabel'] = true
                    
                    stepsArray.push(toPush);
                }

            } else {
                
                for(var month=0; month<=11; month++, index++) {
                    var date = new Date(year, month, 1);
                    var quarter = parseInt(month/3);
                    var value = date.getTime()/1000;

                    if(prevValueBottom === value) {
                        vm.valueBottom = index;
                    }
                    
                    if(prevValueTop === value) {
                        vm.valueTop = index;  
                    } 
                    
                    var toPush = {
                        value: value,
                        quarters: false,
                        label: month,
                        year: year
                    };
                    
                    if(month === 0) toPush['yearLabel'] = true
                    
                    stepsArray.push(toPush);
                }

            }
            years.push(year);
        }

        var initialValues = [];
        initialValues[0] = prevValueBottom > stepsArray[0].value ? vm.valueBottom : 0;
        initialValues[1] = prevValueTop < stepsArray[stepsArray.length-1].value ? vm.valueTop : stepsArray.length-1 ;

        var options = {
            values: initialValues,
            options : {
                step: 2,
                stepsArray: stepsArray,
                showTicksValues: true,
                translate: function(value) {
                    value = stepsArray[value];
                    var date = new Date(value.value*1000);

                    return {'quarters': value.quarters, 'year': date.getFullYear(), 'month': date.getMonth(), 'label': value.label, 'yearLabel': value.yearLabel};
                },
                onChange: function(id, value1, value2) {
                    
                    vm.valueBottom = value1;

                    if(value2 == 0) {
                        vm.timeline.values[1] = 1;
                        vm.valueTop = stepsArray[1];
                        value2 = 1;
                    }
                    if(value1 == stepsArray.length-1) {
                        var last = stepsArray.length - 1;
                        vm.timeline.values[0] = last-1;
                        vm.valueBottom = stepsArray[last-1];
                        value1 = last-1;
                    }
                    if(Math.abs(value2-value1)<1) {
                        vm.valueTop = value1+1;
                        vm.timeline.values[1] = value1+1;
                    } else {
                        vm.valueTop = value2;
                    }
                    
                    var time_1 = stepsArray[value1];
                    var time_2 = stepsArray[value2];

                    vm.graph.setTimeline([time_1.value, time_2.value]);
                }
            },
            style: {
                width: percentage+'%',
            },
            years: years
        };
        return options;
    }
        
    function initGraph() {
        
        var colors = d3.scale.category20();
        if(vm.graph) {
            vm.graph.clear();
            delete vm.graph;
        }

        var options = {
            width: 2000,
            height: 1500,
            color: d3.scale.category20(),
            nodeSize: 5000,
            lineWidth: 1,
            fontSize: 8,
            linkDistance: 25,
            charge: 500,
            gravity: 0.5,
            activeCentrality: 0,
            prominant: false,
            showContent: false
        };
        vm.config = options;

        vm.graph = new Graph(options);
        
        vm.graph.setOnNodeClick(function(d) {
            $('.collapse').collapse('hide');

            $('.collapse-detail').addClass('in'); // hide detail section on the left panel
            $('.collapse-detail').collapse('show'); // show detail section on the left panel

            vm.details['Name'] = d.name;
            vm.details['Category'] = vm.categories[d.group];
            vm.details['Centrality'] = d.centrality;
            vm.details['Start date'] = d.startDate;
            vm.details['End date'] = d.endDate;
            vm.details['Incoming Links'] = d.linksData.incoming;
            vm.details['Outgoing Links'] = d.linksData.outgoing;
            vm.details['Indirect Links'] = d.linksData.indirect;
            vm.details['Total Links'] = d.linksData.total;

            $scope.$apply();
        });

        vm.graph.setOnNodeDeselect(function() {
            $('.collapse-detail').collapse('hide');
            vm.details = {};

            $scope.$apply();
        });

        vm.graph.setOnLinkClick(function(data) {
            $('.collapse').collapse('hide');

            $('.collapse-detail').addClass('in'); // hide detail section on the left panel
            $('.collapse-detail').collapse('show'); // show detail section on the left panel
            
            vm.details = data;
            $scope.$apply();
        })

        vm.graph.draw();

        vm.config.maxCentrality = vm.graph.getMaximalCentralityValue();
        vm.config.activeCentrality = vm.config.maxCentrality/2;

        vm.tagsGroup = vm.graph.getPossibleTags();
        vm.attrGroup = vm.graph.getPossibleAttrs();
        vm.selectedAttrs = {};
        vm.selectedTags = {};
        var extent = vm.graph.getTimeExtent()
        vm.graph.setTimeline([extent.extent[0], extent.extent[1]]);

        vm.timelineMode = 'quarter'
        vm.valueTop = 0;
        vm.valueBottom = 0;

        vm.timeline = yearSelector(extent.years[0],extent.years[1], vm.timelineMode);
        vm.years = vm.timeline.years;
        vm.startYear = vm.timeline.years[0];
        vm.endYear = vm.timeline.years[vm.timeline.years.length-1];
        vm.timelineFilter = false;

        vm.categories = vm.graph.data.groups;
        vm.colors = vm.categories.map(function(d) { return colors(d); });
        vm.styles = vm.colors.map(function(d) { return {'background-color': d}; });
        vm.changeCategoryVisibility = changeCategoryVisibility;
        
        vm.setAllTags = setAllTags;
        vm.setAllAttrs = setAllAttrs;

        vm.backToEarth = backToEarth;

        vm.categoriesVisibility = vm.categories.map(function(d) {return true;});
        
        vm.plainNodes = vm.graph.data.nodes.map(function(d) {
            return { name:d.name, active: true, index: d.index, group: d.group }; 
        });

        vm.nodes = vm.plainNodes.reduce(function(prev, next) {
            if (!(next.group in prev)) {
                prev[next.group] = [];
            }
            
            prev[next.group].push(next);
            return prev;
        }, []);
        vm.details = {};
    }

    function activate() {
        vm.ARCmode = false;
        vm.manageMode = false;
        vm.insightMode = false;
        vm.filterItemsMode = false;

        vm.popoverTemplate = 'graphPopover.html';

        vm.changeMetric = changeMetric;
        vm.showContent = showContent;
        vm.prominantMode = prominantMode;
        vm.changeActiveValue = changeActiveValue;
        vm.showContent = showContent;
        vm.attrMode = attrMode;
        vm.tagMode = tagMode;

        vm.shiftLeft = shiftLeft;
        vm.shiftRight = shiftRight;

        vm.initInsightMode = initInsightMode;
        vm.changeColors = changeColors;

        vm.changeNodesVisibility = changeNodesVisibility;

        vm.changeRange = changeRange;
        vm.filterItems = filterItems;
    }

    function filterItems(word) {
        if(!word || word.length == 0 || word === '')
            vm.filterItemsMode = false;
        else 
            vm.filterItemsMode = true;
    }

    function backToEarth() {
        vm.filterText = '';
        vm.filterTagText = '';
        vm.filterAttrText = '';

        vm.insightMode = false;
        vm.graph.clear();
    }

    function changeCategoryVisibility(id, toogle) {
        vm.graph.setCategoryVisibility(id, toogle);
    }

    function setAllAttrs(toggle) {
        if(!toggle) {
            for(var i in vm.attrGroup) {
                vm.selectedAttrs[vm.attrGroup[i]] = toggle;
            }
            vm.graph.refuseTagging()
        } else {
            vm.attrMode(vm.graph, vm.selectedAttrs);
        }        
    }

    function setAllTags(toggle) {
        if(!toggle) {
            for(var i in vm.tagsGroup) {
                vm.selectedTags[vm.tagsGroup[i]] = toggle;
            }
            vm.graph.refuseTagging()
        } else {
            vm.tagMode(vm.graph, vm.selectedTags);
        }
    }

    function changeColors(id) {
        var group = '.group_' + id;
        vm.graph.changeCategoryColor(vm.colors[id], group);
    }
    
    function initInsightMode() {
        vm.insightMode = true;
        initGraph();
    }

    function showContent(graph, model) {
        vm.graph.showContent(model);
    }

    function prominantMode(graph, mode, currentValue) {
        if(mode) {
            graph.highlightActiveNodes();
        } else {
            graph.highlightAsUsual();
        }
    }

    function changeActiveValue(graph, mode, currentValue) {
        if(mode) {
            graph.setActiveValue(currentValue);
        }
    }

    function tagMode(graph, selectedTags) {
        //graph.refuseTagging();
        if(vm.allTagsSelected) {
        
            var selected = [];

            for(var tagName in selectedTags) {
                if(selectedTags[tagName] === true) {
                    selected.push(tagName);
                }
            }

            graph.switchTagMode(selected);
        }
    } 

    function attrMode(graph, selectedAttrs) {
        //graph.refuseTagging();
    
        if(vm.allAttrsSelected) {

            var selected = [];
            for(var attrName in selectedAttrs) {
                if(selectedAttrs[attrName] === true) {
                    selected.push(attrName);
                }
            }

            graph.switchAttrMode(selected);
        }
    } 

    function changeNodesVisibility() {
        var visibleNodes = vm.nodes.reduce(function(prev, next) { 
            return prev.concat(next.filter(function(d) {return d.active === true;}).map(function(d) {return d.index})); 
        }, []);
        
        var invisibleNodes = vm.nodes.reduce(function(prev, next) { 
            return prev.concat(next.filter(function(d) {return d.active === false;}).map(function(d) {return d.index})); 
        }, []);

        vm.graph.setNodesVisibility(invisibleNodes, false);
        vm.graph.setNodesVisibility(visibleNodes, true);
    }

    function changeMetric(metric, value, graph) {
        switch(metric) {
            case 'nodeSize': 
                graph.changeNodeSize(value);
                break;
            case 'linkWidth':
                graph.changeLineWidth(value);
                break;
            case 'fontSize':
                graph.changeFontSize(value);
                break;
            case 'gravity':
                value = value/100;
                graph.changeGravity(value);
                break;
            case 'charge':
                graph.changeCharge(value);
                break;
            case 'linkDistance':
                graph.changeLineLength(value);
                break;
        }
    }

    activate();
}