$(document).ready(function() {
    var options = {
        width: 2000,
        height: 1500,
        color: d3.scale.category20(),
        nodeSize: 5000,
        lineWidth: 1,
        fontSize: 8,
        linkDistance: 25,
        charge: -1000,
        gravity: 0.5,
        active: 0
    };


    
    $('#link-tooltip').css('display', 'none')
    $('.input_range').slider({
        orientation: "horizontal",
        range: "min"
    });
    $('#element_size').slider({
        min: 1,
        max: 100000,
        value: options.nodeSize,
        slide: setElementSize,
        change: setElementSize
    });
    $('#connection_width').slider({
        min: 1,
        max: 10,
        value: options.lineWidth,
        slide: setLineWidth,
        change: setLineWidth
    });
    $('#connection_length').slider({
        min: 0,
        max: 200,
        value: options.linkDistance,
        slide: setConnectionLength,
        change: setConnectionLength
    });
    $('#font_size').slider({
        min: 1,
        max: 50,
        value: options.fontSize,
        slide: setFontSize,
        change: setFontSize
    });
    $('#gravity').slider({
        min: 1,
        max: 200,
        value: options.gravity * 200,
        slide: setGravity,
        change: setGravity
    });
    $('#charge').slider({
        min: 10,
        max: 2000,
        value: options.charge,
        slide: setCharge,
        change: setCharge
    });
    $('#centrality-active-node').slider({
        min: 1,
        max: 1000,
        value: options.active,
        slide: setActiveValue,
        change: setActiveValue
    });

    $('#tag-grouping').on('change', function() {
        var checked = $(this).prop('checked');
        if(checked){
            graph.switchTagMode([
                         'Data Architecture ',
                         'Application Architecture ',
                         "Business Architecture ",
                         'Technology Architecture ',
                         "Consistence Way To Create A Quote Offer ",
                         "No Consistency In Calculating Quote Rate Across Product lines ",
                         "No Customers Recognition Across Product Lines ",
                         "Immediate Recognition Of Customers Regardless Of Product Lines ",
                         "Common Handling Of All Account Information For All Product Lines ",
                         "Consistence Way To Create A Quote Offer "]);
        } else {
            graph.refuseTagging();
        }
    });
    
    $('.disable-cluster').click(function() {
        graph.refuseTagging();
        $('.possible-tags').prop('checked', false)
        $('.possible-attrs').prop('checked', false)
    })
    
    var tagsToAppend = [];
    var attrsToAppend = [];

    /*var possibleTags = graph.getPossibleTags();
    for(var i in possibleTags) {
        $('.tags-menu').append("<label style='width:150px;' for='"+i+"'>"+possibleTags[i]+"</label><input  style='position:absolute; right:0px;' class='possible-tags' type='checkbox' id='"+i+"'><br/>");
        $('.possible-tags').each(function() {
            $(this).on('change', function() {
                tagsToAppend = [];
                $('.possible-attrs').prop('checked', false)

                $('.possible-tags')
                    .filter(function() {
                        return $(this).prop('checked');
                    })
                    .each(function() {
                        var id = $(this).attr('id')
                        var tag = possibleTags[id];
                        tagsToAppend.push(tag)
                    })

                graph.switchTagMode(tagsToAppend);
            })
        })
    }

    var possibleAttrs = graph.getPossibleAttrs();
    for(var i in possibleAttrs) {
        $('.attrs-menu').append("<label style='width:150px;' for='possible-attrs-"+i+"'>"+possibleAttrs[i]+"</label><input class='possible-attrs' style='position:absolute; right:0px;' type='checkbox' id='possible-attrs-"+i+"'><br/>");

        $('.possible-attrs').each(function() {
            $(this).on('change', function() {
                attrsToAppend = [];
                $('.possible-tags').prop('checked', false)
                $('.possible-attrs')
                    .filter(function() {
                        return $(this).prop('checked');
                    })
                    .each(function() {
                        var id = $(this).attr('id').slice(15);
                        var tag = possibleAttrs[+id];
                        attrsToAppend.push(tag)
                    })

                graph.switchAttrMode(attrsToAppend);
            })
        })
    }
    */
    $('#attr-grouping').on('change', function() {
        var checked = $(this).prop('checked');
        graph.switchAttrMode(['Claim Status', 'Claim Information', 'Customer Information', ]);
    });

    Timeline(graph);

    $('#stop-graph').on('click', function() {
        graph.stopGraph();
    })

    $('#start-graph').on('click', function() {
        graph.startGraph();
    })

    $('#show-content-checkbox').on('change', function() {
        graph.showContent($(this).prop('checked'));
    })

    $('#switch-all-nodes-visible').on('change', function() {
        var checked = $(this).prop('checked');
        graph.switchAllCategories(checked);
        $('.category-visibility').each(function() {
            $(this).prop('checked',checked);
        })
    })

    function setActiveValue() {
        var size = $('#centrality-active-node').slider('value');
        graph.setActiveValue(size);
    }
    function setElementSize() {
        var size = $('#element_size').slider('value');
        graph.changeNodeSize(size);
    }
    function setLineWidth() {
        var width = $('#connection_width').slider('value');
        graph.changeLineWidth(width);
    }
    function setConnectionLength() {
        var length = $('#connection_length').slider('value');
        graph.changeLineLength(length);
    }
    function setFontSize() {
        var size = $('#font_size').slider('value');
        graph.changeFontSize(size);
    }
    function setGravity() {
        var gravity = $('#gravity').slider('value');
        gravity = gravity / 100
        graph.changeGravity(gravity);
    }
    function setCharge() {
        var charge = $('#charge').slider('value');
        charge = charge * -1;
        graph.changeCharge(charge);
    }

    setSizeGraphContainer();

    $(window).resize(function() {
        setSizeGraphContainer();
    });

    $('#settings_panel .block > .title').bind('click', function() {
        $(this).parent().find('.elements_wrap').slideToggle();
        var i = $(this).find('i');
        if (i.hasClass('fa-chevron-down')) {
            i.remove();
            $(this).append('<i class="fa fa-chevron-up"></i>');
        }
        if (i.hasClass('fa-chevron-up')) {
            i.remove();
            $(this).append('<i class="fa fa-chevron-down"></i>');
        }
    });

    var categories = graph.getCategories();
    var color = d3.scale.category20();
    for (var i = 0; i < categories.length; i ++) {
        var html = '<div class="element"><label><span class="inner">' + categories[i] 
            + '</span></label><div class="cp_input" data-id="' + i 
            + '"><div style="background: ' + color(i) + '"></div></div><input class="category-visibility" type="checkbox" data-id="'+i+'" id="category-'+i+'" checked/></div></div>';
        $('.elements_wrap.cpick').append(html);
    }

    $('.category-visibility').on('change', function() {
        var categoryId = $(this).attr('data-id')
        graph.setCategoryVisibility(categoryId, this.checked);
    })

    $('.cp_input').each(function() {
        var self = $(this);
        var bg = self.find('div').css('background-color');

        self.spectrum({
            color: bg,
            change: function(color) {
                var colorHex = color.toHexString(),
                    id = self.data('id');
                self.find('div').css('background', colorHex);
                changeCategoryColor(colorHex, id);
            }
        });
    });

    function changeCategoryColor(color, id) {
        var group = '.group_' + id;
        graph.changeCategoryColor(color, group);
    }
});


function setSizeGraphContainer() {
    var wh = $(window).height(),
        ww = $(window).width();
    if ($('#settings_panel').hasClass('active')) {
        ww = $(window).width() - 300;
    }

    $('#graph').css({ width: ww, height: wh }).scrollLeft(wh/*/2*/).scrollTop(wh/1.5);
    //$('#settings_panel').css({ height: wh });
}
function toggleSettingsPanel() {
    var ww = $(window).width();
    if (!$('#settings_panel').hasClass('active')) {
        $('#graph').animate({
            width: ww - 300
        }, 300);
        $('#settings_panel').animate({
            marginRight: 0
        },{
            duration: 300,
            complete: function() {
                $('#settings_panel').addClass('active')
            }
        });
        $('#settings_icon').animate({
            right: 330
        }, 300);
    }
    else {
        $('#graph').animate({
            width: ww
        }, 300);
        $('#settings_panel').animate({
            marginRight: -300
        },{
            duration: 300,
            complete: function() {
                $('#settings_panel').removeClass('active')
            }
        });
        $('#settings_icon').animate({
            right: 30
        }, 300);
    }
}
function showSettingsPopup() {
    if ($('#settings_panel').hasClass('active')) {
        $('#settings_panel').hide().removeClass('active');
    }
    else {
        $('#settings_panel').show().addClass('active');
    }
}