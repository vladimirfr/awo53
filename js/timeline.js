function Timeline(graph) {
    var step = 'monthly';
    var currentPosition = 0;
    var percentage = 0;
    
    function setTimeline(event, ui) {
        var time = $("#timeline").slider('value');
        console.log(ui.values);
        graph.setTimeline([ui.values[0], ui.values[1]]);
    }

    var extent = graph.getTimeExtent()
    for(var i=extent.years[0]; i<=extent.years[1];i++) {
        $('#min-timeline-year').append('<option>'+i+'</option>')
        $('#max-timeline-year').append('<option>'+i+'</option>')
    }
    $('#max-timeline-year').val(extent.years[1]);
    
    function yearSelector(first, last, mode) {
        var _first = (new Date(0))
        var _last = (new Date(0));
        _first.setFullYear(first)
        _first = _first.getTime()/1000;
        _last.setFullYear(last);
        _last = _last.getTime()/1000;

        $('#timeline .ticks *').remove()
        $('#timeline').slider({
            min: _first,
            max: _last,
            values: [_first, _last+24*30*60*60],
            range: true,
            step: 24*60*60*30,
            slide: setTimeline,
            change:  setTimeline
        });
        
        percentage = (30*(last-first));

        $('#timeline').css('width', (30*(last-first))+'%');
        var a = 100/(last-first)
        for(var i=first; i<=last; i++) {
            var _i = i - first
            // Years ticks
            $('#timeline .ticks').append(
                "<div style='position:absolute; bottom: 16px; width:80px;  margin-left:-40px; text-align:center; left: "+(_i*a)+"%'>"+i+"</div>"+
                "<div style='position:absolute; top: -15px; border-left: 6px solid white; height:30px; left: "+(_i*a)+"%'></div>"              
            )
            // quarter ticks
            if(i!=last) {
                for(var j=0; j<4; j++) {
                    $('#timeline .ticks').append(
                        "<div style='position:absolute; bottom: -30px; width:80px;  margin-left:-40px; text-align:center; left: "+(_i*a+j*(a/4))+"%'>Q"+(j+1)+"</div>"+
                        "<div style='position:absolute; top: -10px; border-left: 3px solid white; height:20px; left: "+(_i*a+j*(a/4))+"%'></div>"              
                    )   
                    // montly ticks
                    if(mode == 'monthly') {
                        for(var k=1; k<3; k++) {
                            $('#timeline .ticks').append(
                                "<div style='position:absolute; top: -5px; border-left: 3px solid white; height:10px; left: "+(_i*a+j*(a/4)+k*(a/12))+"%'></div>"
                            );
                        }
                    }
                }
            }
        }
        $('#timeline').css('right', '-0%');
    }
    
    graph.setTimeline([extent.extent[0], extent.extent[1]]);
    
    yearSelector(extent.years[0],extent.years[1]);
    
    $('.button-right').on('click', function() {
        if(currentPosition-90>-percentage)
            currentPosition-=30;

        $('#timeline').animate({'left': currentPosition+'%'});
    });

    $('.button-left').on('click', function() {
        if(currentPosition<0)
            currentPosition+=30;

        $('#timeline').animate({'left': currentPosition+'%'});
    });

    $('#min-timeline-year').change(function() {
        var _min = this.value;
        var _max = $('#max-timeline-year').val();
        $('#max-timeline-year option').remove()
        for(var i=+_min+1; i<=extent.years[1]; i++)
            $('#max-timeline-year').append('<option>'+i+'</option>')
        $('#max-timeline-year').val(Math.max(_max, _min));

        yearSelector(_min, Math.max(_max, _min));
    })

    $('#max-timeline-year').change(function(){
        var _min = $('#min-timeline-year').val();
        var _max = this.value;
        $('#min-timeline-year option').remove()

        for(var i=extent.years[0]; i<+_max; i++)
            $('#min-timeline-year').append('<option>'+i+'</option>')
        $('#min-timeline-year').val(Math.min(_max, _min));

        yearSelector(Math.min(_max, _min), _max);
    })
    $('#timeline-steps').change(function () {
        var step = $(this).val();
        $('#timeline').slider({
            step: step == 'monthly'? 24*60*60*30: 24*60*60*90 
        })
        yearSelector($('#min-timeline-year').val(), $('#max-timeline-year').val(), step);
    });
    $('#show-active-nodes-checkbox').on('change', function() {
        if($(this).prop('checked')) {
            $('#centrality-slider').removeAttr('style')
            graph.highlightActiveNodes();
        }
        else {
            graph.highlightAsUsual();
            $('#centrality-slider').css('display', 'none')
        }
    });
}
